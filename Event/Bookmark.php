<?php

namespace XLabs\BookmarkBundle\Event;

use Symfony\Component\EventDispatcher\Event;

class Bookmark extends Event
{
    const NAME = 'xlabs_bookmark.event';

    protected $bookmark;

    public function __construct($bookmark)
    {
        $this->bookmark = $bookmark;
    }

    public function getBookmark()
    {
        return $this->bookmark;
    }
}