<?php

namespace XLabs\BookmarkBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use XLabs\BookmarkBundle\Engines\Bookmark as BookmarkEngine;
use XLabs\BookmarkBundle\Entity\Bookmark;
use Symfony\Component\Console\Helper\ProgressBar;
use \DateTime;
use \Exception;

class InitialBackupCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('xlabs:bookmark:initial_backup')
            ->setDescription('Puts all redis data into MySQL')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getApplication()->getKernel()->getContainer();
        $em = $container->get('doctrine.orm.default_entity_manager');

        $output->writeln('');
        $output->writeln('<comment>:::::::::::::  BACK UP BOOKMARKS  :::::::::::::</comment>');
        $output->writeln('<comment>::::::::::::::: Redis --> MySQL :::::::::::::::</comment>');
        $output->writeln('');

        $bundle_config = $container->getParameter('xlabs_bookmark_engine');
        if(array_key_exists('backup', $bundle_config) && is_array($bundle_config['backup']) && count($bundle_config['backup']))
        {
            $bookmarkable_entities = $bundle_config['backup'];
        } else {
            dump('No entities to be backed up found on the bundle configuration.');
            exit;
        }

        $bookmark_engine = $container->get(BookmarkEngine::class);

        $bookmarks_counter = 0;
        foreach($bookmarkable_entities as $key => $bookmarkable_entity)
        {
            $qb = $em->createQueryBuilder();
            $entities = $qb
                ->select('e')
                ->from($bookmarkable_entity, 'e')
                ->getQuery();
            $progress = new ProgressBar($output, count($entities->getArrayResult()));
            $progress->start();
            $progress->setMessage('Redis Bookmarks ['.$bookmarkable_entity.']');
            $progress->setFormat('[<info>%bar%</info>] %current%/%max% <comment>%message%</comment> %percent:3s%%');
            foreach($entities->iterate() as $e)
            {
                $e = $e[0];

                // Get bookmarkers
                $bookmarkers = $bookmark_engine->getBookmarkers($key, $e->getId(), $showScores = true, $maxResults = false);
                if(count($bookmarkers))
                {
                    foreach($bookmarkers as $bookmarker_id => $score)
                    {
                        $now = new DateTime();
                        $score = $now->setTimestamp($score);
                        $bookmark = new Bookmark($bookmarker_id, $key, $e->getId(), $score);
                        $em->persist($bookmark);
                        $bookmarks_counter++;
                        /*try {
                            $em->flush();
                        } catch(Exception $exception) {

                        }*/
                    }
                }

                if($bookmarks_counter % 100 == 0)
                {
                    $em->flush();
                    $em->clear();
                }

                $progress->advance();
            }
            $em->flush();

            $progress->finish();
        }
    }
}