<?php

namespace XLabs\BookmarkBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use XLabs\BookmarkBundle\Engines\Bookmark as BookmarkEngine;
use XLabs\BookmarkBundle\Entity\Bookmark;
use Symfony\Component\Console\Helper\ProgressBar;

class RestoreCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('xlabs:bookmark:restore')
            ->setDescription('Puts all MySQL data into redis. Optional user ids to restore only theirs.')
            ->addArgument('user_ids', InputArgument::IS_ARRAY, 'User ID')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getApplication()->getKernel()->getContainer();
        $em = $container->get('doctrine.orm.default_entity_manager');

        $bookmark_engine = $container->get(BookmarkEngine::class);

        $qb = $em->createQueryBuilder();
        $qb
            ->select('e')
            ->from(Bookmark::class, 'e');

        $user_ids = $input->getArgument('user_ids') ? $input->getArgument('user_ids') : false;
        if($user_ids)
        {
            $qb->where(
                $qb->expr()->in('e.user_id', $user_ids)
            );
        }
        $entities = $qb->getQuery();

        $output->writeln('');
        $output->writeln('<comment>::::::::::::: RESTORING BOOKMARKS :::::::::::::</comment>');
        $output->writeln('<comment>::::::::::::::: MySQL --> Redis :::::::::::::::</comment>');
        $output->writeln('');

        $progress = new ProgressBar($output, count($entities->getArrayResult()));
        $progress->start();
        $progress->setMessage('Bookmarks');
        $progress->setFormat('[<info>%bar%</info>] %current%/%max% <comment>%message%</comment> %percent:3s%%');
        foreach($entities->iterate() as $bookmark)
        {
            $bookmark = $bookmark[0];

            $bookmark_engine->setUser(array('id' => $bookmark->getUserId()))->add($bookmark->getBookmarkedType(), $bookmark->getBookmarkedId(), $bookmark->getScore()->getTimestamp(), false);

            $progress->advance();
        }

        $progress->finish();

        $output->writeln('');
        $output->writeln('<comment>::::::::::::::::::::::: DONE ::::::::::::::::::::::</comment>');

        /*$bookmarks = $em->getRepository(Bookmark::class)->findAll();
        if($bookmarks)
        {
            foreach($bookmarks as $bookmark)
            {
                $bookmark_engine->setUser(array('id' => $bookmark->getUserId()))->add($bookmark->getBookmarkedType(), $bookmark->getBookmarkedId(), $bookmark->getScore()->getTimestamp());
            }
        }*/
    }
}