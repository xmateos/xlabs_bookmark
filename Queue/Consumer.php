<?php

namespace XLabs\BookmarkBundle\Queue;

use XLabs\RabbitMQBundle\RabbitMQ\Consumer as Parent_Consumer;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use XLabs\BookmarkBundle\Entity\Bookmark;
use \DateTime;
use \Exception;

class Consumer extends Parent_Consumer
{
    // set your custom consumer command name
    protected static $consumer = 'xlabs:bookmark:mysql_backup';

    // following function is required as it is
    protected function configure()
    {
        $this
            ->setName(self::$consumer)
        ;
    }

    // following function is required as it is
    public function getQueueName()
    {
        return Producer::getQueueName();
    }

    public function callback($msg)
    {
        // this is all sample code to output something on screen
        $container = $this->getApplication()->getKernel()->getContainer();

        $msg = json_decode($msg->body);

        $action = $msg->action;
        $user_id = $msg->user_id;
        $bookmarked_type = $msg->bookmarked_type;
        $bookmarked_id = $msg->bookmarked_id;
        $score = $msg->score;

        //$em = $container->get('doctrine.orm.default_entity_manager');
        $em = $this->getEntityManager();
        switch($action)
        {
            case 'bookmark':
                $now = new DateTime();
                $score = $now->setTimestamp($score);
                $bookmark = new Bookmark($user_id, $bookmarked_type, $bookmarked_id, $score);
                $em->persist($bookmark);
                break;
            case 'unbookmark':
                $bookmark = $em->getRepository(Bookmark::class)->findOneBy(array(
                    'bookmarked_id' => $bookmarked_id,
                    'bookmarked_type' => $bookmarked_type,
                    'user_id' => $user_id
                ));
                if($bookmark)
                {
                    $em->remove($bookmark);
                }
                break;
        }
        try {
            $em->flush();
        } catch (Exception $e) {

        }
    }

    private function getEntityManager()
    {
        $container = $this->getApplication()->getKernel()->getContainer();
        $em = $container->get('doctrine.orm.default_entity_manager');
        if(!$em->isOpen())
        {
            $em = $em->create($em->getConnection(), $em->getConfiguration());
        }
        return $em;
    }
}