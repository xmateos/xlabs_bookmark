<?php

namespace XLabs\BookmarkBundle\Extension;

use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use XLabs\BookmarkBundle\Engines\Bookmark as BookmarkEngine;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class BookmarkExtension extends AbstractExtension
{
    private $token_storage;
    private $xlabs_bookmark_engine;

    public function __construct(TokenStorageInterface $token_storage, BookmarkEngine $xlabs_bookmark_engine)
    {
        $this->token_storage = $token_storage;
        $this->xlabs_bookmark_engine  = $xlabs_bookmark_engine;
    }
    
    public function getFunctions()
    {
        return array(
            new TwigFunction('isBookmark', array($this, 'isBookmark')),
            new TwigFunction('getBookmarks', array($this, 'getBookmarks')),
            new TwigFunction('getBookmarked', array($this, 'getBookmarked')),
            new TwigFunction('getTotalUserBookmarks', array($this, 'getTotalUserBookmarks')),
        );
    }
    
    public function getFilters()
    {
        return array();
    }

    public function isBookmark($bookmarkType, $bookmark_id)
    {
        $user = $this->token_storage->getToken()->getUser();
        return is_string($user) ? false : $this->xlabs_bookmark_engine->isBookmark($bookmarkType, $bookmark_id);
    }

    public function getBookmarks($bookmarkType, $bookmark_id)
    {
        return $this->xlabs_bookmark_engine->getTotalBookmarks($bookmarkType, $bookmark_id);
    }

    public function getBookmarked($bookmarkType)
    {
        return $this->xlabs_bookmark_engine->getBookmarks($bookmarkType);
    }

    public function getTotalUserBookmarks($bookmarkType)
    {
        $user = $this->token_storage->getToken()->getUser();
        return is_string($user) ? false : $this->xlabs_bookmark_engine->getTotalBookmarked($bookmarkType);
    }
}