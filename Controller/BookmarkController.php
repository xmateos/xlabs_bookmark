<?php

namespace XLabs\BookmarkBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use XLabs\BookmarkBundle\Event\Bookmark as BookmarkEvent;

class BookmarkController extends Controller
{
    /**
     * @Route("/example", name="xlabs_bookmark_example")
     */
    public function exampleAction()
    {
        return $this->render('XLabsBookmarkBundle:Bookmark:example.html.twig');
    }

    /**
     * @Route("/action", name="xlabs_bookmark_ajax", options={"expose"=true})
     */
    public function bookmarkAction()
    {
        $request = $this->get('request_stack')->getCurrentRequest();

        $data_xlabs_bookmark = stripslashes(file_get_contents("php://input"));
        $data_xlabs_bookmark = json_decode($data_xlabs_bookmark, true);
        $data_xlabs_bookmark = $data_xlabs_bookmark['data-xlabs-bookmark'];

        $user = $this->container->get('security.token_storage')->getToken()->getUser();
        $response = array(
            'currentStatus' => 0
        );
        $xlabs_bookmark_engine = $this->get('xlabs_bookmark_engine');
        if($request->isMethod('POST') && $xlabs_bookmark_engine && !is_string($user))
        {
            $response['currentStatus'] = $xlabs_bookmark_engine->switchBookmarkStatus($data_xlabs_bookmark['entityType'], $data_xlabs_bookmark['entity_id']);

            // Dispatch event
            $event = new BookmarkEvent(array(
                'bookmarker_id' => $user->getId(),
                'bookmarkedType' => $data_xlabs_bookmark['entityType'],
                'bookmarked_id' => $data_xlabs_bookmark['entity_id'],
                'timestamp' => time(),
                'currentStatus' => $response['currentStatus']
            ));
            $this->get('event_dispatcher')->dispatch(BookmarkEvent::NAME, $event);
        }
        $response['currentTotalBookmarks'] = $xlabs_bookmark_engine->getTotalBookmarks($data_xlabs_bookmark['entityType'], $data_xlabs_bookmark['entity_id']);

        $response = new Response(json_encode($response, JSON_PRETTY_PRINT), 200);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }
}
