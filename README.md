A redis driven bookmark engine.

## Installation ##

Install through composer:

```bash
php -d memory_limit=-1 composer.phar require xlabs/bookmarkbundle
```

This bundle depends on "xlabs/rabbitmqbundle". Make sure to set it up too.

In your AppKernel

```php
public function registerbundles()
{
    return [
    	...
    	...
    	new XLabs\BookmarkBundle\XLabsBookmarkBundle(),
    ];
}
```

```bash
php bin/console doctrine:schema:update --force
```

## Routing

Append to main routing file:

``` yml
# app/config/routing.yml
  
x_labs_bookmark:
    resource: "@XLabsBookmarkBundle/Resources/config/routing.yml"
    #prefix:   /
```

## Configuration sample
Default values are shown below:
``` yml
# app/config/config.yml
  
x_labs_bookmark:
    redis_settings:
        host: 192.168.5.23
        port: 6379
        database_id: 7
    _key_namespace: 'xlabs:bookmark'
    backup: # for mysql backup/restore
        <alias>: <entity_FQCN>
```

### Usage ###
Append this anywhere in you template
```php
{% include 'XLabsBookmarkBundle:Bookmark:loader.html.twig' %}
```

To see a sample template, check:
```php
XLabsBookmarkBundle:Bookmark:example.html.twig
```

### MySQL Backup ###
Make sure you run the following command. This is the consumer that will save all operations in the project DB.
``` bash
php bin/console xlabs:bookmark:mysql_backup --no-debug
```
If ever Redis lost all the data, you will be able to recover it by issueing the following command:
``` bash
php bin/console xlabs:bookmark:restore --no-debug
```
If you already have data in redis and want to create a mysql backup of it, there´s a sample one-time command you should copy to your project and adapt conveniently:
``` bash
php bin/console xlabs:bookmark:initial_backup --no-debug
```

### Event listener ###
If you want some action to take place whenever a BOOKMARK takes place in the frontends, you can create an event listener as follows:
``` yml
# YourBundle/Resources/config/services.yml
    ...
    xlabs_bookmark.event_listener:
        class:  YourBundle\EventListeners\YourListener.php
        tags:
            - { name: kernel.event_listener, event: xlabs_bookmark.event, method: yourListenerMethod }
```
```php
use Symfony\Component\EventDispatcher\Event;
  
class YourListener extends Event
{
    public function yourListenerMethod(Event $event)
    {
        dump($event->getBookmark()); die;
    }
}
```
The $event variable contains all info about the BOOKMARK action that has taken place.

### Tweaking ###
By default, the service uses the user in session. If you ever wanted to use the service on your own by using any specific user performing the BOOKMARK action:
```php
$user = $em->getRepository('YourBundle:YourUserEntity')->find(<ID>);
$bookmark_engine = $container->get('xlabs_bookmark_engine');
$bookmark_engine->setUser($user);
$bookmark_engine->...
```