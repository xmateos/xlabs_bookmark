<?php

namespace XLabs\BookmarkBundle\Engines;

use Predis\Client as Predis;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use XLabs\BookmarkBundle\Queue\Producer as MysqlBackup;
use XLabs\BookmarkBundle\Entity\User;

class Bookmark
{
    private $config;
    private $redis;
    protected $user_in_session;
    private $mysql_backup;

    public function __construct($config, TokenStorageInterface $token_storage, MysqlBackup $mysql_backup)
    {
        $this->config = $config;
        $this->redis = new Predis(array(
            'scheme' => 'tcp',
            'host'   => $config['redis_settings']['host'],
            'port'   => $config['redis_settings']['port'],
            'database' => $config['redis_settings']['database_id']
        ), array(
            'prefix' => isset($config['_key_namespace']) ? $config['_key_namespace'].':' : ''
        ));
        $this->user_in_session = $token_storage->getToken() ? $token_storage->getToken()->getUser() : false;
        $this->mysql_backup = $mysql_backup;
    }

    public function disableLogging()
    {
        /*
         * For bulk operations, call this method to avoid PHP memory leaks
         */
        $this->redis->getConnection()->setLogger(null);
        return $this;
    }

    public function getInstance()
    {
        return $this->redis;
    }

    public function getAllKeys($redis_instance = false)
    {
        $redis_instance = $redis_instance ? $redis_instance : $this->redis;
        $keyPattern = "*";
        $keys = $redis_instance->keys($keyPattern);
        return $keys;
    }

    public function deleteAllKeys($redis_instance = false)
    {
        $redis_instance = $redis_instance ? $redis_instance : $this->redis;
        $keys = $this->getAllKeys();
        if($keys)
        {
            $prefix = $redis_instance->getOptions()->__get('prefix')->getPrefix();
            foreach($keys as $key)
            {
                $key = substr($key, strlen($prefix));
                $redis_instance->del(array($key));
            }
        }
    }

    public function changePrefix($old_prefix, $new_prefix)
    {
        $redis['generic'] = new Predis([
            'scheme' => 'tcp',
            'host'   => $this->config['redis_settings']['host'],
            'port'   => $this->config['redis_settings']['port'],
            'database' => $this->config['redis_settings']['database_id']
        ]);
        $keys = $this->getAllKeys($redis['generic']);
        if($keys)
        {
            foreach($keys as $key)
            {
                if(strlen(strstr($key, $old_prefix)) > 0)
                {
                    $redis['generic']->rename($key, str_replace($old_prefix, $new_prefix, $key));
                }
            }
        }
        $redis['generic']->disconnect();
    }

    public function setUser($user)
    {
        if(is_array($user))
        {
            $u = $user;
            $user = new User();
            $user->id = $u['id'];
        }
        $this->user_in_session = $user;
        return $this;
    }

    public function add($bookmarkType, $bookmark_id, $score = false, $backup = true)
    {
        $score = $score ? $score : time();
        $this->redis->zAdd("user:".$this->user_in_session->getId().":bookmarks:".$bookmarkType, array($bookmark_id => $score));
        $this->redis->zAdd($bookmarkType.":".$bookmark_id.":bookmarked:user", array($this->user_in_session->getId() => $score));
        $this->redis->zIncrBy("ranking:".$bookmarkType, 1, $bookmark_id);
        if($backup)
        {
            $this->mysql_backup->process(array(
                'action' => 'bookmark',
                'user_id' => $this->user_in_session->getId(),
                'bookmarked_type' => $bookmarkType,
                'bookmarked_id' => $bookmark_id,
                'score' => $score,
            ));
        }
        $currentStatus = 1;
        return $currentStatus;
    }

    public function remove($bookmarkType, $bookmark_id, $backup = true)
    {
        $this->redis->zRem("user:".$this->user_in_session->getId().":bookmarks:".$bookmarkType, $bookmark_id);
        $this->redis->zRem($bookmarkType.":".$bookmark_id.":bookmarked:user", $this->user_in_session->getId());
        $this->redis->zIncrBy("ranking:".$bookmarkType, -1, $bookmark_id);
        if($backup)
        {
            $this->mysql_backup->process(array(
                'action' => 'unbookmark',
                'user_id' => $this->user_in_session->getId(),
                'bookmarked_type' => $bookmarkType,
                'bookmarked_id' => $bookmark_id,
                'score' => false,
            ));
        }
        $currentStatus = 0;
        return $currentStatus;
    }

    /**
     * User has bookmarked $bookmarkType with ID $bookmark_id
     */
    public function isBookmark($bookmarkType, $bookmark_id)
    {
        return is_null($this->redis->zScore("user:".$this->user_in_session->getId().":bookmarks:".$bookmarkType, $bookmark_id)) ? false : true;
    }

    /**
     * Get all user bookmarks of $bookmarkType type
     */
    public function getBookmarks($bookmarkType, $maxResults = false, $page = 1)
    {
        $offset = $maxResults ? (($page - 1)*$maxResults) : 0;
        $lastItem = $maxResults ? ($offset + ($maxResults - 1)) : -1;
        return $this->redis->zRevRange("user:".$this->user_in_session->getId().":bookmarks:".$bookmarkType, $offset, $lastItem);
    }

    /**
     * Get all users that bookmarked $bookmarkType with ID $bookmark_id
     */
    public function getBookmarkers($bookmarkType, $bookmark_id, $showScores = false, $maxResults = false, $page = 1)
    {
        $offset = $maxResults ? (($page - 1)*$maxResults) : 0;
        $lastItem = $maxResults ? ($offset + ($maxResults - 1)) : -1;

        return $showScores ? $this->redis->zRange($bookmarkType.":".$bookmark_id.":bookmarked:user", $offset, $lastItem, 'withscores') : array_filter($this->redis->zRange($bookmarkType.":".$bookmark_id.":bookmarked:user", $offset, $lastItem), function($v){
            return $v != 'undefined';
        });

        return $this->redis->zRange($bookmarkType.":".$bookmark_id.":bookmarked:user", 0, -1);
    }

    public function getBookmarkersByScore($bookmarkType, $bookmark_id, $min_score, $max_score, $showScores = false, $maxResults = false, $page = 1)
    {
        $offset = $maxResults ? (($page - 1)*$maxResults) : 0;
        $count = $maxResults ? $maxResults : -1;

        return $showScores ? $this->redis->zRangeByScore($bookmarkType.":".$bookmark_id.":bookmarked:user", $min_score, $max_score, array('withscores' => true, 'limit' => array($offset, $count))) : array_filter($this->redis->zRangeByScore($bookmarkType.":".$bookmark_id.":bookmarked:user", $offset, $count), function($v){
            return $v != 'undefined';
        });
    }

    /**
     * Get total bookmarks made on $bookmarkType with ID $bookmark_id
     */
    public function getTotalBookmarks($bookmarkType, $bookmark_id)
    {
        return $this->redis->zCard($bookmarkType.":".$bookmark_id.":bookmarked:user");
    }

    /**
     * Get total bookmarks made by a user on $bookmarkType
     */
    public function getTotalBookmarked($bookmarkType)
    {
        return $this->redis->zCard("user:".$this->user_in_session->getId().":bookmarks:".$bookmarkType);
    }

    public function switchBookmarkStatus($bookmarkType, $bookmark_id, $score = false)
    {
        return $this->isBookmark($bookmarkType, $bookmark_id) ? $this->remove($bookmarkType, $bookmark_id) : $this->add($bookmarkType, $bookmark_id, $score);
    }

    public function getMostBookmarked($bookmarkType, $maxResults = false, $page = 1, $showScores = false)
    {
        $offset = $maxResults ? (($page - 1)*$maxResults) : 0;
        $lastItem = $maxResults ? ($offset + ($maxResults - 1)) : -1;
        return $showScores ? $this->redis->zRevRange("ranking:".$bookmarkType, $offset, $lastItem, 'withscores') : $this->redis->zRevRange("ranking:".$bookmarkType, $offset, $lastItem);
    }
}