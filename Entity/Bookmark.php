<?php

namespace XLabs\BookmarkBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use \DateTime;

/**
 * @ORM\Entity
 * @ORM\Table(name="xlabs_bookmarks", uniqueConstraints={@ORM\UniqueConstraint(name="unique_row",columns={"bookmarked_id", "bookmarked_type", "user_id"})}, indexes={@ORM\Index(name="search_idx", columns={"bookmarked_id", "bookmarked_type", "user_id"})})
 */
class Bookmark
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    public function getId()
    {
        return $this->id;
    }

    /**
     * @ORM\Column(name="bookmarked_id", type="integer", nullable=false)
     */
    protected $bookmarked_id;

    public function getBookmarkedId()
    {
        return $this->bookmarked_id;
    }

    public function setBookmarkedId($bookmarked_id)
    {
        $this->bookmarked_id = $bookmarked_id;
    }

    /**
     * @ORM\Column(name="bookmarked_type", type="string", length=64, nullable=false)
     */
    protected $bookmarked_type;

    public function getBookmarkedType()
    {
        return $this->bookmarked_type;
    }

    public function setBookmarkedType($bookmarked_type)
    {
        $this->bookmarked_type = $bookmarked_type;
    }

    /**
     * @ORM\Column(name="score", type="datetime", nullable=false)
     */
    private $score;

    public function getScore()
    {
        return $this->score;
    }

    public function setScore(DateTime $score)
    {
        $this->score = $score;
    }

    /*
     * @ORM\ManyToOne(targetEntity="XLabs\LikeBundle\Model\XLabsLikeUserInterface")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     **/
    /**
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     */
    protected $user_id;

    public function getUserId()
    {
        return $this->user_id;
    }

    //public function setUser(XLabsLikeUserInterface $user)
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
    }

    public function __construct($user_id, $bookmarked_type, $bookmarked_id, $score)
    {
        $this->user_id = $user_id;
        $this->bookmarked_type = $bookmarked_type;
        $this->bookmarked_id = $bookmarked_id;
        $this->score = $score;
    }
}