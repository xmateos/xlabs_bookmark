<?php

namespace XLabs\BookmarkBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class BookmarkControllerTest extends WebTestCase
{
    public static $user = array(
        'PHP_AUTH_USER' => 'testuser',
        'PHP_AUTH_PW'   => 'testtest',
    );

    public function testBookmarkAction()
    {
        $client = static::createClient();

        $client->request('POST', $client->getContainer()->get('router')->generate('xlabs_bookmark_ajax'), array(
            json_encode(array(
                'data-xlabs-bookmark' => array(
                    'entityType' => 'testEntity',
                    'entity_id' => 1
                )
            ))
        ), array(), self::$user);
        $response = $client->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        $response = json_decode($response->getContent(), true);
        $this->assertArrayHasKey('currentStatus', $response);
        $this->assertInternalType('int', $response['currentStatus']);
        $first_response = $response['currentStatus'];

        $client->request('POST', $client->getContainer()->get('router')->generate('xlabs_bookmark_ajax'), array(
            json_encode(array(
                'data-xlabs-bookmark' => array(
                    'entityType' => 'testEntity',
                    'entity_id' => 1
                )
            ))
        ), array(), self::$user);
        $response = $client->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        $response = json_decode($response->getContent(), true);
        $this->assertArrayHasKey('currentStatus', $response);
        $this->assertInternalType('int', $response['currentStatus']);

        $this->assertEquals(!$first_response, $response['currentStatus']);
    }
}
